package src.main.java.br.com.marconemm.exercicios.marcone;

import src.main.java.br.com.marconemm.exemplos.poo.Animal;
import src.main.java.br.com.marconemm.exemplos.poo.Cachorro;
import src.main.java.br.com.marconemm.exemplos.poo.Gato;

public class Dia12a14_PrincipiosDePOOMarcone {

    public static void main(String[] args) {

        Cachorro rex = new Cachorro();
        Cachorro lulu = new Cachorro("Lulu", "Poodle", "branca");

        rex.setPropriedadeEncapsulada("Foi atribuido um valor a propriedade encapsulada. Da forma correta.");
        rex.raca = "Teste raça";

        System.out.println(rex.getPropriedadeEncapsulada());
        rex.caminhar();
        rex.abanaOrabo();

        Gato mimo = new Gato();
        mimo.setNome("Mimo");
        mimo.pelagem = "Teste de pelagem";

        System.out.println(mimo.getNome());
        mimo.caminhar();
        mimo.abanaOrabo();

        System.out.println("\n=================\n");

        Gato felix = new Gato();
        felix.setNome("Felix");

        lulu.comer("ração");
        felix.comer("ração gourmet");

        // Polimofirmos de coerção — exemplos:
        Gato gatoXX = new Gato();
        Animal gatoYY = new Gato();

        gatoXX.abanaOrabo(); // Método exclusivo da classe Gato.
        gatoYY.caminhar(); // Método generico da classe Animal.
    }
}
