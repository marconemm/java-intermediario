package src.main.java.br.com.marconemm.exercicios.marcone;

public class Dia2_ConstantesExerciciosMarcone {
    final private static double PI = 3.141592653;
    final private static float GRAVIDADE = 9.80665f;
    final private static int VELOCIDADE_LUZ = 299792458;
    final private static String SEPARADOR = "\n===========\n";

    private static void calcularAreaDeCirculoComRaio(float raioEmCm) {
        //A = π*r²
        double area = PI * Math.pow(raioEmCm, 2);

        System.out.printf("A área de um círculo com o raio %.0fcm é %.2fcm%n", raioEmCm, area);
    }

    private static void calcularEnergiaCinetica(float vInicialEmKmPorHora, float massaEmQuilos, float alturaEmMetros) {
        /*
        Sabendo que:
        A fórmula da energia cinética é Ec = mv²/2, onde Ec — energia cinética em ‘joules’, m — massa em kg
        e v — velocidade em m/s;
        Que para converter de km/h para m/s a razão é de km/h / 3,6; e
        Que para se descobrir o tempo da queda a fórmula é: tempo = √(2 * distância / aceleração) + (Vinicial / aceleração)

        Resolva:
        Imagine um objeto de massa <massaEmQuilos> kg em queda livre próximo à superfície da Terra e partindo do
        repouso ou não  (Vi >= 0m/s), numa altura inicial de <alturaEmMetros> metros.
        Calcule a energia cinética do objeto no instante em que ele atinge o solo (Vf = 0m/s).
        */
        final boolean temVelocidadeInicial = vInicialEmKmPorHora > 0;

        // t = √(2d / a) + (v₀ / a)
        final float TEMPO_DA_QUEDA = temVelocidadeInicial
                ? (float) Math.sqrt(2 * alturaEmMetros / GRAVIDADE + (vInicialEmKmPorHora / GRAVIDADE))
                : (float) Math.sqrt(2 * alturaEmMetros / GRAVIDADE);

        // Vf = Vi + a * t
        final float V_FINAL = (float) (vInicialEmKmPorHora / 3.6) + GRAVIDADE * TEMPO_DA_QUEDA;

        // Ec = mv²/2
        final double E_CINETICA = (massaEmQuilos * Math.pow(V_FINAL, 2)) / 2;

        if (temVelocidadeInicial)
            System.out.printf("A energia cinética, ao atingir o solo, de um corpo arremessado na Terra a %.3fKm/h de" +
                            " uma altura de %.2fm e com massa de %.3fKg é de: %.4f joules.%n",
                    vInicialEmKmPorHora, alturaEmMetros, massaEmQuilos, E_CINETICA
            );
        else
            System.out.printf("A energia cinética, ao atingir o solo, de um corpo partindo do repouso na Terra" +
                            " de uma altura de %.2fm e com massa de %.3fKg é de: %.4f joules.%n",
                    alturaEmMetros, massaEmQuilos, E_CINETICA
            );
    }

    private static void tempoViagemDaLuz(float distanciaEmKm) {
        // Qual o tempo de viagem da luz entre dois pontos distantes <distanciaEmKm>km um do outro?

        // t = d / v
        final float TEMPO = (float) (distanciaEmKm * 1000) / VELOCIDADE_LUZ;

        System.out.printf("A luz leva %.4fs para percorrer uma distância de %.2fkm%n", TEMPO, distanciaEmKm);
    }


    public static void main(String[] args) {
        calcularAreaDeCirculoComRaio(15);
        calcularAreaDeCirculoComRaio(20.3456f);
        calcularAreaDeCirculoComRaio(1.987654332f);
        calcularAreaDeCirculoComRaio(1000f);
        System.out.println(SEPARADOR);

        calcularEnergiaCinetica(0, 5, 20);
        calcularEnergiaCinetica(0, 650, 9);
        calcularEnergiaCinetica(3, 107, 3);
        calcularEnergiaCinetica(3, 60.8f, 3);
        System.out.println(SEPARADOR);

        tempoViagemDaLuz(0.1f);
        tempoViagemDaLuz(100);
        tempoViagemDaLuz(40075); // circunferência da Terra.
        tempoViagemDaLuz( 384400); // distância entre Terra e Lua.
    }
}