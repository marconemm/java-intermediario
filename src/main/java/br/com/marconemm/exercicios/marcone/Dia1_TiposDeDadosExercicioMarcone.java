package src.main.java.br.com.marconemm.exercicios.marcone;

public class Dia1_TiposDeDadosExercicioMarcone {
/* 1. Declarando e Inicializando Variáveis:
        1. Crie um programa que declare variáveis de cada tipo primitivo em Java (int, double, char, boolean)
        e as inicialize com valores diferentes.
        2. Exiba o valor de cada variável utilizando o comando `System.out.println()`

    2. Crie um programa que converta um valor do tipo `int` para `double` e vice-versa.
*/

    public static void main(String[] args) {
        imprimir();
    }

    private static void imprimir() {
        System.out.println("1. Declarando e Inicializando Variáveis:");
        imprimirValores();

        System.out.println("\n2. Crie um programa que converta um valor do tipo `int` para `double` e vice-versa.");
        System.out.println("O valor de 123, como double é: " + parseIntegerToDouble(123));
        System.out.println("O valor de 765, como double é: " + parseIntegerToDouble(765));
        System.out.println("O valor de 4.9876543456789, como Integer é: " + parseDoubleToInteger(4.9876543456789));
        System.out.println("O valor de 45.9876, como Integer é: " + parseDoubleToInteger(45.9876));
    }

    private static void imprimirValores() {
        int valor1 = 1;
        double pontoFlutuante = 10.12451234;
        char caracter = 'M';
        boolean flag = true;

        System.out.println("valor1 = " + valor1);
        System.out.println("pontoFlutuante = " + pontoFlutuante);
        System.out.println("caracter = " + caracter);
        System.out.println("flag = " + flag);
    }

    private static double parseIntegerToDouble(int valor) {
        return Double.parseDouble(String.valueOf(valor));
    }

    private static int parseDoubleToInteger(double valor) {
        return ((Double) valor).intValue();
    }
}
