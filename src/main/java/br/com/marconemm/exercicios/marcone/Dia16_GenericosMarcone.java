package src.main.java.br.com.marconemm.exercicios.marcone;

import src.main.java.br.com.marconemm.exemplos.poo.Cachorro;
import src.main.java.br.com.marconemm.exemplos.poo.Gato;
import src.main.java.br.com.marconemm.exemplos.poo.PetShop;

public class Dia16_GenericosMarcone {

    public static void main(String[] args) {
        PetShop<Cachorro> petShopDeCachorros = new PetShop<>();
        PetShop<Gato> petShopDeGatos = new PetShop<>();

//        PetShop<Coelho> petShopDeCoelhos = new PetShop<>(); // não é possível, pois Coelho não estende Animal.

        petShopDeCachorros.addAnimalParaAtendimento(new Cachorro("Totó", "Vira-latas", "braca"));
        petShopDeGatos.addAnimalParaAtendimento(new Gato());

        // Sintaxe mais verbosa:
        for (Cachorro cachorro : petShopDeCachorros.getListaDeAnimaisEmAtendimento()) {
            System.out.println(cachorro.getNome());
        }

        // Sintaxe lambda:
        petShopDeCachorros.getListaDeAnimaisEmAtendimento().forEach(cachorro -> cachorro.caminhar());

        // Sintaxe de referência:
        petShopDeCachorros.getListaDeAnimaisEmAtendimento().forEach(Cachorro::abanaOrabo);

        for (Gato gato : petShopDeGatos.getListaDeAnimaisEmAtendimento()) {
            gato.abanaOrabo();
        }



    }
}
