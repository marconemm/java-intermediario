package src.main.java.br.com.marconemm.exercicios.marcone;

import src.main.java.br.com.marconemm.exemplos.poo.Cachorro;

public class Dia11_ClassesObjetosMarcone {

    public static void main(String[] args) {

        Cachorro rex = new Cachorro();
        Cachorro lulu = new Cachorro("Lulu", "Poodle", "branca");

        System.out.println(rex.getNome());
        System.out.println(lulu.getNome());

        rex.latir();
        lulu.latir();

        rex.brincar("osso");
        lulu.brincar("bola");
    }
}
