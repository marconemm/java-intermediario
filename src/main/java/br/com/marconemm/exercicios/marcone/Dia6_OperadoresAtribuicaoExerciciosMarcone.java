package src.main.java.br.com.marconemm.exercicios.marcone;

import java.util.Scanner;

public class Dia6_OperadoresAtribuicaoExerciciosMarcone {

    /*Cenário: Um sistema de gestão de estoque precisa atualizar o saldo de produtos após cada venda.
    Crie um programa que receba a quantidade de produtos vendidos e utilize operadores de atribuição para atualizar o
    saldo de estoque de um produto específico. Apresente o saldo atualizado ao utilizador.
    */

    private static final Camisa camisa = new Camisa();
    private static final Bermuda bermuda = new Bermuda();
    private static final Chapeu chapeu = new Chapeu();
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int opcao;

        while (true) {
            System.out.println("Qual produto deseja comprar?");
            System.out.println("1-) Camisa\n2-) Bermuda\n3-) Chapéu\n0-) Finalizar");
            opcao = scanner.nextInt();

            if (opcao == 0)
                break;

            switch (opcao) {
                case 1:
                    System.out.println("Quantas camias você quer levar?");
                    opcao = scanner.nextInt();
                    camisa.qtdEstoque -= opcao;
                    break;
                case 2:
                    System.out.println("Quantas bermudas você quer levar?");
                    opcao = scanner.nextInt();
                    bermuda.qtdEstoque -= opcao;
                    break;
                case 3:
                    System.out.println("Quantos chapéus você quer levar?");
                    opcao = scanner.nextInt();
                    chapeu.qtdEstoque -= opcao;
                    break;
                default:
                    System.out.println("Por favor, escolha uma opção válida.");
            }
        }

        if (camisa.houveVenda() || bermuda.houveVenda() || chapeu.houveVenda()) {
            System.out.println("\nParabéns! Nós conseguimos vender:");
            System.out.println("Camisa: " + (camisa.ESTOQUE_INICIAL - camisa.qtdEstoque));
            System.out.println("Bermudas: " + (bermuda.ESTOQUE_INICIAL - bermuda.qtdEstoque));
            System.out.println("Chapéus: " + (chapeu.ESTOQUE_INICIAL - chapeu.qtdEstoque));
        } else
            System.out.println("Infelizmente não conseguimos vender nada.");

        System.out.println("\nE o nosso estoque ficou assim:");
        System.out.println("Camisa(s): " + camisa.qtdEstoque);
        System.out.println("Bermuda(s): " + bermuda.qtdEstoque);
        System.out.println("Chapéu(s): " + chapeu.qtdEstoque);
    }

    // Classes Auxiliares:
    public static class Camisa {
        public final int ESTOQUE_INICIAL = 50;
        public int qtdEstoque = 50;

        public boolean houveVenda() {
            return ESTOQUE_INICIAL != qtdEstoque;
        }
    }

    public static class Bermuda {
        public final int ESTOQUE_INICIAL = 50;
        public int qtdEstoque = 50;

        public boolean houveVenda() {
            return ESTOQUE_INICIAL != qtdEstoque;
        }
    }

    public static class Chapeu {
        public final int ESTOQUE_INICIAL = 50;
        public int qtdEstoque = 50;

        public boolean houveVenda() {
            return ESTOQUE_INICIAL != qtdEstoque;
        }
    }
}