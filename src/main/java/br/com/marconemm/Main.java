package src.main.java.br.com.marconemm;

import src.main.java.br.com.marconemm.exemplos.TiposDeDados;

public class Main {
    public static void main(String[] args) {
        // System.out.println("Começamos!");

        TiposDeDados tiposDeDados = new TiposDeDados();
        long limite = 999999999L;

        tiposDeDados.performancePrimitivo(limite);
        tiposDeDados.performanceNaoPrimitivo(Long.valueOf(limite));
    }
}
