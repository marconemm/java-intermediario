package src.main.java.br.com.marconemm.exemplos;

public class Escopos {
    /*
     * Escopos: são blocos de código que delimitam o acesso às variáveis que neles são declaradas.
     * Exemplos: Ecopo de Classe, Escopo de Método e Escopo de Estrutura Condicional.
     */

    // Escopo de Classe:
    int escopoDeClasse = 0;

    public void testeDeAcessoAEscopoDeClasse(){

        // Escopo de Método:
        int escopoDeMetodo = 1;

        escopoDeClasse = 2; // Métodos têm acesso ao escopo de classe.

        if (true){
            // Escopo de Estrutura Condicional:
            int escopoDeEstruturaCondicional = 2;

            escopoDeClasse = 4; // Estruturas condicionais têm acesso ao escopo de classe.
            System.out.println(escopoDeMetodo); // Estruturas condicionais têm acesso ao escopo de método.
            System.out.println(escopoDeEstruturaCondicional); // Estruturas condicionais têm acesso ao escopo de estrutura condicional.
        }

        // escopoDeEstruturaCondicional = 0; // Métodos NÃO têm acesso ao escopo de estrutura condicional.
    }
}
