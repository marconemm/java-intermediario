package src.main.java.br.com.marconemm.exemplos;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StramsAPIs {
    public static void main(String[] args) {

        final List<String> listaDeNomes = Arrays.asList("Adriana", "João", "Maria", "Pedro", "Ana", "Jéssica", "Marcelo");
        final List<Integer> listaDeNumeros = Arrays.asList(1, 2, 3, 4, 5, 6, 33, 78, 199);

        // Filtra nomes com mais de 5 letras e mapeia para maiúsculo
        List<String> nomesMaiusculos = listaDeNomes.stream()
                .filter(nome -> nome.length() > 5)
                .map(String::toUpperCase)
                .collect(Collectors.toList());

        System.out.println(nomesMaiusculos); // Imprime: [ADRIANA, JÉSSICA, MARCELO]
        System.out.println("\n==========\n");

        int somaQuadrados = listaDeNumeros.stream()
                .mapToInt(numero -> numero * numero)
                .sum();

        // Imprime: A soma dos quadrados da lista [1, 2, 3, 4, 5, 6, 33, 78, 199] é: 46865
        System.out.println("A soma dos quadrados da lista " + listaDeNumeros + " é: " + somaQuadrados);

    }
}
