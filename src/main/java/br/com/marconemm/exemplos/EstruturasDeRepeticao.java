package src.main.java.br.com.marconemm.exemplos;

public class EstruturasDeRepeticao {

    public static void lacoFor() {
        for (int limiteFor = 0; limiteFor < 5; limiteFor++) {
            System.out.println("limiteFor = " + (limiteFor + 1));
        }
    }

    public static void lacoWhile() {
        boolean flag = true; // condição de interrupção do laço while.
        int limiteWhile = 0;

        while (flag) {
            System.out.println("limiteWhile = " + ++limiteWhile);

            if (limiteWhile > 5)
                flag = false;
        }
    }

    public static void lacoDoWhile() {
        boolean condicao = true; // condição de interrupção do laço while.
        int doWhile = 0;

        do {
            System.out.println("doWhile = " + ++doWhile);

            if (doWhile > 5)
                condicao = false;

        } while (condicao);
    }

    public static void parOuImpar(int numero) {

        if (numero % 2 == 0) {
            System.out.println(numero + " é par");
        } else {
            System.out.println(numero + " é ímpar");
        }
    }

    public static void calculaMedia() {
        //               0,  1,  2,   3,  4,  5 (índices)
        int[] listaDeNumeros = {10, 20, 30, 40, 50, 60};
        int soma = 0;

        for (int numero : listaDeNumeros)
            // soma = soma + numero;
            soma += numero;


        double media = (double) soma / listaDeNumeros.length;
        System.out.println("A média dos números é: " + media);
    }

    public static void main(String[] args) {
//        lacoFor();
//        lacoWhile();
//        lacoDoWhile();

//        parOuImpar(10);
//        parOuImpar(11);
//        parOuImpar(1873253);

        calculaMedia();

    }
}
