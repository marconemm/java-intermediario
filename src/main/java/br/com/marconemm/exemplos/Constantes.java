package src.main.java.br.com.marconemm.exemplos;

public class Constantes {
    /*
    Em Java, a palavra-chave final é utilizada para declarar uma constante, ou seja, um valor que não pode ser
    alterado após a sua inicialização. As constantes são úteis para representar valores fixos e imutáveis nos seus
    programas, como o número pi (3.14159) ou a velocidade da luz (299.792.458 m/s).
    */

    final double PI = 3.14159; // Constante do tipo double
    final int VELOCIDADE_LUZ = 299792458; // Constante do tipo int
    final String NOME_CONSTANTE = "Valor fixo"; // Constante do tipo String

    public void teste(){
        // PI = 4; // Ao descomentar essa linha, a classe para de compilar por erro de sintaxe.
    }
}
