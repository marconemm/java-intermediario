package src.main.java.br.com.marconemm.exemplos;

import src.main.java.br.com.marconemm.exemplos.interfaces.IMultiplosParametros;
import src.main.java.br.com.marconemm.exemplos.poo.Animal;
import src.main.java.br.com.marconemm.exemplos.poo.Cachorro;
import src.main.java.br.com.marconemm.exemplos.poo.Gato;
import src.main.java.br.com.marconemm.exemplos.poo.PetShop;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class ParadigmaFuncional {
    public static void main(String[] args) {
        final ParadigmaFuncional PF = new ParadigmaFuncional();

        /*
         * 1-) () -> instrução única
         *
         * 2-) parametro -> instrucao unica, consumindo o "parametro"
         *
         * 3-) (parametro1, parametro2) -> instrucao unica, consumindo os "parametro1" e "parametro2"
         *
         * 4-) (parametro1, parametroN) -> {
         * instrução;
         * instrução;
         * instrução;
         *
         * return resultado da função;
         * }
         * */

        PetShop<Animal> petShop = new PetShop<>();

        petShop.addAnimalParaAtendimento(new Cachorro("Totó"));
        petShop.addAnimalParaAtendimento(new Cachorro("Apolo"));
        petShop.addAnimalParaAtendimento(new Gato("Fifi"));
        petShop.addAnimalParaAtendimento(new Gato("Félix"));

        petShop.getListaDeAnimaisEmAtendimento().forEach(animal -> System.out.println(animal.getNome()));
        System.out.println("\n ========== \n");

        IMultiplosParametros<Integer, Integer, Integer, Integer> multiplosParametros = (a, b, c) -> a + b + c;
        System.out.println(multiplosParametros.soma(1, 2, 3)); // Resultado: 6
        System.out.println("Retorno: " + multiplosParametros.teste(90));
        System.out.println("\n ========== \n");


        petShop.getListaDeAnimaisEmAtendimento().forEach(Animal::darApata);
        System.out.println("\n ========== \n");


        Consumer<Integer> imprimirNumero = System.out::println;
        imprimirNumero.accept(5); // Resultado: 5 (impresso no console)
        System.out.println("\n ========== \n");

        Function<String, String> toUpperCase = String::toUpperCase;
        String resultado = toUpperCase.apply("olá, mundo!"); // Resultado: "OLÁ, MUNDO!"
        System.out.println(resultado);
        System.out.println("\n ========== \n");

        Predicate<Integer> oNumeroEpar = (n) -> n % 2 == 0;
        Arrays.asList(new Integer[]{1, 45, 367, 334, 566, 3333, 3, 8})
                .forEach(numero -> {
                    System.out.print("O número " + numero);
                    boolean ePar = oNumeroEpar.test(numero); // Resultado: true

                    if (ePar)
                        System.out.println(" é par.");
                    else
                        System.out.println(" é ímpar.");
                });
    }
}


























