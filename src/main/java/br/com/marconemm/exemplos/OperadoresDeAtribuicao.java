package src.main.java.br.com.marconemm.exemplos;

public class OperadoresDeAtribuicao {
    public static void exemplosAtribuicaoComSoma(){
        String exemplo = "Primeiro exemplo";

        System.out.println(exemplo);

        // exemplo = "Primeiro exemplo" + ". E o primeiro exemplo acava aqui.";
        exemplo += ". E o primeiro exemplo acava aqui.";

        System.out.println(exemplo);
    }

    public static void exemplosAtribuicaoComSubtracao(){
        float valor = 10f;

        System.out.println("valor = " + valor);

        // valor = 10f - 2f;
        valor -= 2.8f;

        System.out.println("valor = " + valor);

    }

    public static void exemplosAtribuicaoComMultiplicacao(){
        float valor = 100.1f;

        System.out.println("valor = " + valor);

        // valor = 100.1f x 0.9f;
        valor *= 0.9f;

        System.out.println("valor = " + valor);

    }

    public static void exemplosAtribuicaoComDivisao(){
        double valor = 100;

        System.out.println("valor = " + valor);

        // valor = 100 / 3;
        valor /= 3;

        System.out.println("valor = " + valor);

    }

    public static void exemplosAtribuicaoComResto(){
        int resto = 100;

        System.out.println("resto = " + resto);

        // resto = 100 % 3 = 1;
        resto %= 3;

        System.out.println("resto = " + resto);

    }

    public static void exemploAtribuicaoCombinada(){
        int y = 2;
        //      5  +    4    +   1
        int x = 5 + y * 2 + y / 2;
        System.out.println("x = " + x);

        //      5   +     4  *    1
        int z = 5 + y * 2 * y / 2;
        System.out.println("z = " + z);

        //    (   2º     )    (  1º )
        //  (              3º        )
        z = ((5 + (y * 2)) * (y / 2));
        System.out.println("z = " + z);
    }

    public static void exemplosAtribuicaoCoPotencia(){
        double poetencia = 100;

        System.out.println("poetencia = " + poetencia);

        // poetencia = 100 ^ 3 = 1000000.0;
        poetencia = Math.acos(2);

        System.out.println("poetencia = " + poetencia);

    }

    public static void exemplosIncrementoEdecrementoPosFixacao(){
        int contador = 5;

        System.out.println(contador++); // Imprime 5 e depois incrementa contador para 6
        System.out.println(contador); // Imprime 6

        int outroContador = 10;

        System.out.println(outroContador--); // Imprime 10 e depois decrementa outroContador para 9
        System.out.println(outroContador); // Imprime 9


    }

    public static void exemplosIncrementoEdecrementoPreFixacao(){
        int x = 3;

        System.out.println(x + ++x); // Imprime 6 (x + 4), incrementa x para 4 e realiza a operação matemática.
        System.out.println(x); // Imprime 4

        int y = 8;

        System.out.println(y - --y); // Imprime 1 (y - 7) decrementa y para 7 e realiza a operação matemática.
        System.out.println(y); // Imprime 7
    }

    public static void main(String[] args) {
        exemplosIncrementoEdecrementoPosFixacao();
        exemplosIncrementoEdecrementoPreFixacao();
    }
}
