package src.main.java.br.com.marconemm.exemplos;

public class Fatorial {
    public static void main(String[] args) {
        Fatorial fatorial = new Fatorial();

        System.out.println(fatorial.calculaFatorialDe(3)); // 3 * 2 * 1 * 1 = 6
    }

    /**
     * Calcula o valor do fatorial para um número qualquer positivo.
     *
     * @param x - valor que será calculado o fatorial.
     * @return O valor do fatorial.
     */
    public long calculaFatorialDe(int x) {
        // Se x for igual a 0 (zero) então retorna 1.
        if (x == 0) {
            System.out.println("1 = ");
            return 1;
        }

    /* Para qualquer outro número, calcula o seu valor multiplicado
       pelo fatorial de seu antecessor. */
        System.out.println(x + " * ");
        return x * calculaFatorialDe(x - 1);
    }
}