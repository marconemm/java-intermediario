package src.main.java.br.com.marconemm.exemplos;

import src.main.java.br.com.marconemm.utils.Cronometro;

public class TiposDeDados {
    // Tipos primitivos:
    byte valorNumericoInteiroMinusculo = 1;
    short valorNumericoInteiroCurto = 1;
    int valorNumericoInteiro = 0;
    long valoNumericoInteirolongo = 0L;
    char caracter = 'A';
    boolean verdadeiroFalso = true;
    double numeroDecimalLongo = 1.32154732145732145732132157455346257432546325234567321;
    float numeroDecialCurto = 1.61652346512f;

    // Tipos Não Primitivos, wrappers:
    Byte valorNumericoInteiroMinusculoNP = 1;
    Short valorNumericoInteiroCurtoNP = 2;
    Integer valorNumericoInteiroNP = 3;
    Long valoNumericoInteirolongoNP = 4L;
    Character caracterNP = 'B';
    Boolean verdadeiroFalsoNP = false;
    Double numeroDecimalLongoNP = 2.16213124123245625432542524354325432543253;
    Float numeroDecialCurtoNP = 2.61652346512f;

    public void exemplo() {
        // atribuição imutável (dado primitivo):
        valorNumericoInteiroMinusculo = (byte) (valorNumericoInteiroMinusculo + 1);

        // atribuição mutável (dado não primitivo):
        valorNumericoInteiroMinusculoNP = Byte.parseByte(valorNumericoInteiroMinusculoNP + 1 + "");
    }

    public void performancePrimitivo(long limite) {
        System.out.println("\nTestando a performance de uma contagem até " + limite + " com tipo primitivo:");
        Cronometro.iniciar();
        long valorPrimitivo = 0L;

        for (long i = 0L; i < limite; i++)
            valorPrimitivo = valorPrimitivo + 1L;

        Cronometro.tempoTotal();
    }

    public void performanceNaoPrimitivo(Long limite) {
        System.out.println("\nTestando a performance de uma contagem até " + limite + " com tipo NÃO primitivo:");
        Cronometro.iniciar();
        Long valorPrimitivo = 0L;

        for (Long i = 0L; i < limite; i++)
            valorPrimitivo = valorPrimitivo + 1L;

        Cronometro.tempoTotal();
    }
}
