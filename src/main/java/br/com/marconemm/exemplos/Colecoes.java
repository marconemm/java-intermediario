package src.main.java.br.com.marconemm.exemplos;

import java.util.ArrayList;
import java.util.List;

public class Colecoes {
    public static void main(String[] args) {
        String[] colecaoNomes = new String[10];

        colecaoNomes[0] = "Amanda";
        colecaoNomes[9] = "Carlos";

        System.out.println("Qtd de posições: " + colecaoNomes.length);

        for (int i = 0; i < colecaoNomes.length; i++)
            System.out.println(colecaoNomes[i]);


        ArrayList<String> listaCompleta = new ArrayList<>();
        List<String> listaSimples = new ArrayList<>();

        // Lista completa:
        listaCompleta.clone(); // método criado devido à implementação da Interface "Clonable".
        listaCompleta.ensureCapacity(2); // método exclusivo da classe ArrayList.

        // Lista simples:
        listaSimples.add("teste"); // método criado devido à implementação da Interface "List<T>".
    }
}
