package src.main.java.br.com.marconemm.exemplos;

import java.util.Arrays;

public class Funcoes {

    public static int valor = 1;

    public void exemploPublic(){

    }

    public String exemploPublicString(){
        return "Meu valor";
    }

    protected void exemploProtected(){

    }

    private void exemploPrivate(){

    }

    public static  double calcularMedia(double nota1, double nota2) {
        double media = (nota1 + nota2) / 2;

        return media;
    }

    public static void imprimirMensagem(String mensagem) {
        System.out.println(mensagem);
    }

    private static void teste(){
        Funcoes funcoes = new Funcoes();

        funcoes.exemploPublic();
        funcoes.exemploProtected();
        funcoes.exemploPrivate();
        Funcoes.main(new String[]{"abc", "dfg"});

    }

    public static void main(String[] args) {
        //Modificado de acesso: public, protected e private
        //Public -> permite que o projeto inteiro enxergue essa função
        //Protected -> permite que apenas o pacote onde se encontra a classe enxergue essa função
        //Private -> permite que apenas a própria classe enxergue essa função.

        //void -> retorno vazio ou "não retorna nada".

        System.out.println("args[0] = " + args[0]);
        System.out.println("args[1] = " + args[1]);
        System.out.println("args[2] = " + args[2]);

        System.out.println("Média = " + calcularMedia(9.98, 1.87));

        imprimirMensagem("Olá, mundo!");

    }
}
