package src.main.java.br.com.marconemm.exemplos.poo;

import src.main.java.br.com.marconemm.exemplos.interfaces.IAnimal;

public class Gato extends Animal implements IAnimal {
    public String pelagem;

    public Gato() {}

    public Gato(String nome) {
        super(nome);
    }

    public void caminhar() {
        System.out.println("O gato está caminhado.");
    }

    /**
     * O animal come a sua comida preferida.
     *
     * @param comida a comida preferida do animal.
     */
    public void comer(String comida) {
        System.out.println("O gato " + getNome() + " solicitou garfo e faca...");
        System.out.println(getNome() + " degusta sua " + comida + " com elegância.");
    }

    /**
     * O animal abana o rabo.
     */
    public void abanaOrabo() {
        System.out.println("O gato abana o rabo.");
    }
}
