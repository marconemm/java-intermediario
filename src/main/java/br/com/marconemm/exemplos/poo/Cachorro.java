package src.main.java.br.com.marconemm.exemplos.poo;

import src.main.java.br.com.marconemm.exemplos.interfaces.IAnimal;

public class Cachorro extends Animal implements IAnimal {
    public String raca;

    private String propriedadeEncapsulada;

    public Cachorro() {
        //Construtor da classe.

        // construtor vazio / padrão
        super("Rex", "dourado");
        raca = "Golden Retriever";
        idade = 0;
    }

    public Cachorro(String nome, String raca, String cor, int idade) {
        //Construtor da classe.

        // construtor com parâmetros inciais
        super(nome, cor);
        this.raca = raca;
        this.idade = idade;
    }

    public Cachorro(String nome, String raca, String cor) {
        //Construtor da classe.

        // construtor com parâmetros inciais
        super(nome, cor);
        this.raca = raca;
        idade = 0;
    }

    public Cachorro(String nome) {
        super(nome);
    }

    //getters and setters:
    public String getPropriedadeEncapsulada() {
        //getter:
        return propriedadeEncapsulada;
    }

    public void setPropriedadeEncapsulada(String propriedadeEncapsulada) {
        //setter:
        this.propriedadeEncapsulada = propriedadeEncapsulada.toUpperCase();
    }

    public void latir() {
        System.out.println("Au - au!");
        System.out.println("O cachorro " + getNome() + " latiu!");
    }

    public void correr() {
        System.out.println(getNome() + " corre pelo parque com alegria.");
    }

    public void brincar(String objeto) {
        System.out.println(getNome() + " pega o(a) " + objeto + " e brinca com entusiasmo.");
    }


    /**
     * O animal caminha.
     */
    public void caminhar() {
        System.out.println("O cachorro está caminhado.");
    }

    /**
     * O animal come a sua comida preferida.
     *
     * @param comida a comida preferida do animal.
     */
    public void comer(String comida) {
        System.out.println(getNome() + " devora sua " + comida + " com apetite.");
    }

    /**
     * O animal abana o rabo.
     */
    public void abanaOrabo() {
        System.out.println("O cachorro abana o rabo.");
    }
}
