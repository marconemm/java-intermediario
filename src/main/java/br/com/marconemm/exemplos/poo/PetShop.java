package src.main.java.br.com.marconemm.exemplos.poo;

import java.util.ArrayList;
import java.util.List;

public class PetShop<E extends Animal> {
    private List<E> listaDeAnimaisEmAtendimento;

    public PetShop() {
        this.listaDeAnimaisEmAtendimento = new ArrayList<>();
    }

    public void addAnimalParaAtendimento(E animal) {
        listaDeAnimaisEmAtendimento.add(animal);
    }

    public List<E> getListaDeAnimaisEmAtendimento() {
        return listaDeAnimaisEmAtendimento;
    }
}
