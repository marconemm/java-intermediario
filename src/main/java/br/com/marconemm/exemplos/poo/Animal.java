package src.main.java.br.com.marconemm.exemplos.poo;

import src.main.java.br.com.marconemm.exemplos.interfaces.IAnimal;

public abstract class Animal implements IAnimal {
    private String nome, cor;
    public int idade;

    public Animal(){}

    public Animal(String nome) {
        this.nome = nome;
    }

    public Animal(String nome, String cor) {
        this.nome = nome;
        this.cor = cor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    /**
    * O animal caminha.
    */
    public abstract void caminhar();

    /**
     * O animal come a sua comida preferida.
     *
     * @param comida a comida preferida do animal.
     * */
    public abstract void comer(String comida);

    public void darApata(){
        System.out.println("O animal " + nome + " deu a patinha.");
    }
}
