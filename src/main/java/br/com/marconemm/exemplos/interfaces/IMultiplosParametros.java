package src.main.java.br.com.marconemm.exemplos.interfaces;

@FunctionalInterface
public interface IMultiplosParametros<A, B, C, R> {
    R soma(A a, B b, C c);


    // pode possuir métodos default (implementados)
    default R teste(C c) {
        R r = (R) c;
        System.out.println("O método \"teste\" foi acionado e retornará: " + r);

        return r;
    }
}
