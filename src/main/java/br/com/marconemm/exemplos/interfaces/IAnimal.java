package src.main.java.br.com.marconemm.exemplos.interfaces;

@FunctionalInterface
public interface IAnimal {

    /**
     * O animal abana o rabo.
     * */
    void abanaOrabo();
}
