package src.main.java.br.com.marconemm.exemplos;

public class OperadoresLogicos {

    public static void exemploAulaDia4(){
        int x = 1, y = 2, z = 3;

        if (((x > 5) && (y < 10)) && (z >= 10))

        // ((x > 5) && (y < 10)) (esquerda) &&
        // (z >= 10) (direita)

        //(x > 5) (esquerda) &&
        // (y < 10)  (direita)

        //(falso) (esquerda) &&
        // (y < 10)  (direita)

        {
            // intensionalmente em branco.

        } else {

            System.out.println("Entraria aqui...");
        }
    }

    public static void logicosBinarioEUnario() {
        int x = 5, y = 7, z = 9;

        System.out.println("Demonstrando o operador &&:");
        if ((x > y) && (x++ > z)) ; // intesionalmente em branco.

        else
            System.out.println("Valor de  x: " + x);

        System.out.println("\nDemonstrando o operador &:");
        if ((x > y) & (x++ > z)) ; // intesionalmente em branco.

        else
            System.out.println("Valor de  x: " + x);
    }

    public static void maiorIdade() {
        int idade = 24;

        boolean eMaiorDeIdade = idade >= 18;
        boolean eMaiorDe23 = idade > 23;

        if (eMaiorDeIdade && eMaiorDe23) {
            System.out.println("Você é maior de idade e também tem mais de 23 anos.");

        } else if (eMaiorDeIdade && !eMaiorDe23) {
            System.out.println("Você é maior de idade mas não possui mais de 23 anos.");
        } else {
            System.out.println("Você é menor de idade.");
        }
    }

    public static void verificaLogin() {
        // ForntEnd (entrada usuário(a)):
        String usuarioDigitado = "admin";
        String senhaDigitada = "senha123";

        // Backend:
        String usuario = "admin";
        String senha = "senha123";

        boolean eLoginValido = usuario == usuarioDigitado && senha.equals(senhaDigitada);

        if (eLoginValido) {
            System.out.println("Login efetuado com sucesso!");
        } else {
            System.out.println("Usuário ou senha inválidos.");
        }
    }

    public static void classiFicacaoFinal() {
        int notaProva1 = 75;
        int notaProva2 = 64;

        double media = (notaProva1 + notaProva2) / 2;
        boolean eAprovado = media >= 70;
        String resultado = eAprovado ? "Aprovado" : "Reprovado";

        System.out.println("Resultado: " + resultado + ", pois a sua média foi " + media + ".");
    }

    public static void switchCase() {
        int notaProva1 = 90;
        int notaProva2 = 64;

        switch (notaProva1){
            case 75:
                int outraVariavel = 10;
                System.out.println("Escopo do case 75.");
                System.out.println(outraVariavel);
                break;

            case 90:
                outraVariavel = 11;
                System.out.println("Escopo do case 90.");
//                System.out.println(outraVariavel);
                break;
            default:
                System.out.println("Escopo do default/padrão.");
//                System.out.println(outraVariavel);

        }
    }

    public static void main(String[] args) {
        switchCase();
    }
}

