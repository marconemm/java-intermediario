package src.main.java.br.com.marconemm.utils;

import java.util.concurrent.TimeUnit;

public class Cronometro {
    private static long inicio;

    public static void iniciar() {
        inicio = System.currentTimeMillis();
        System.out.println("Cronômetro iniciado...");
    }

    public static void tempoTotal() {
        final String texto = "O tempo total de execução foi de:";
        final long tempoTotal = (System.currentTimeMillis() - inicio);
        final long minutos = TimeUnit.MILLISECONDS.toSeconds(tempoTotal) / 60;
        final long segundos = TimeUnit.MILLISECONDS.toSeconds(tempoTotal) % 60;
        final long milisegundos = tempoTotal - TimeUnit.MINUTES.toMillis(minutos) - TimeUnit.SECONDS.toMillis(segundos);

        System.out.println("Cronômetro parado.");
        System.out.println(String.format("%s %02d min %02d segundos e %d milisegundos.", texto, minutos,
                segundos, milisegundos)
        );
    }
}